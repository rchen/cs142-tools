This repository contains a number of tools that were created by Roger Chen while CAing for CS142 in Winter 2015.

* assign.rb - Assigns projects for TAs to grade. Sends an email zip to each grader with the projects they need to grade.
* package.rb - Packages up projects for a payload.
