# Packages assignments for graders. Run the following for command line flags
# > ruby package.rb -h

require 'optparse'

SUBMISSIONS_SRC = "/usr/class/cs142/submissions"

# Set up command line options
options = {:project => nil, :file => nil}
OptionParser.new do |opts|
    opts.banner = "Usage: package.rb [options]"
    opts.on("-p PROJECT", "--project PROJECT", "Select project number") do |p|
        options[:project] = p
    end
    opts.on("-f FILE", "--file FILE", "File containing emails of all students") do |f|
        options[:file] = f
    end
    opts.on("-h", "--help", "Displays help") do
        puts opts
        exit
    end
end.parse!

# Read in the file of students and get their SUNet. Assumes that we have emails
students = Array.new
File.open(options[:file], "r") do |f|
    f.each_line do |line|
        students << line.strip.split("@")[0]
    end
end

# Find the last assignment that they turned in for the project
project_base = "#{SUBMISSIONS_SRC}/#{options[:project]}"
assignments = Hash.new
Dir.entries(project_base).select do |f|
    if f != "." and f != ".."
        sunet, submission = f.split("-")
        if students.include?(sunet)
            submission = submission.to_i
            if !assignments.key?(sunet)
                assignments[sunet] = submission
            end
            if assignments[sunet] < submission
                assignments[sunet] = submission
            end
        end
    end
end

# Get all of the assignment paths
assignments_paths = assignments.map { |key, value| "#{project_base}/#{key}-#{value}" }

# Zip up everything into a file called p#{options[:project]}-timestamp.zip
system("zip -r p#{options[:project]}-#{Time.now.to_i}.zip #{assignments_paths.join(" ")}")

