# This script grabs all the submissions from /usr/class/cs142/submissions and
# assigns projects to TAs.

require 'fileutils'
require 'mail'
require 'optparse'
require 'set'

SUBMISSIONS_SRC = "/usr/class/cs142/submissions"
SUBMISSIONS_TARGET = "."
PAYLOADS_TARGET = "payloads"

# Load graders from the graders file
graders = Array.new
File.open("graders", "r").each_line do |line|
    graders << line.strip 
end

# Responsible for taking care of command line arguments
options = {}
optparse = OptionParser.new do |opts|
    opts.banner = "Usage: assign.rb [options]"
    options[:project] = nil
    opts.on("-p", "--project PROJECT", "Select project number") do |p|
        options[:project] = p
    end
    opts.on("-h", "--help", "Help") do |h|
        puts opts
    end
end
optparse.parse!

# Raise error if no project is selected
raise OptionParser::MissingArgument if options[:project].nil?
puts "Selected Project #{options[:project]}"

# Create ./submissions if it does not exist
if (!File.directory?(SUBMISSIONS_TARGET))
    FileUtils.mkdir SUBMISSIONS_TARGET
end

# Copy all submissions corresponding to the current project using rsync
puts "Synchronizing #{SUBMISSIONS_SRC} to your local directory..."
system "rsync -ah #{SUBMISSIONS_SRC} #{SUBMISSIONS_TARGET} --exclude '.git'"

# Generate list of all the students who have submitted projects
projects = Hash.new
Dir.entries("#{SUBMISSIONS_TARGET}/submissions/#{options[:project]}").select do |f|
    if f != "." and f != ".."
        sunet, submission = f.split("-")
        submission = submission.to_i
        if !projects.key?(sunet)
            projects[sunet] = submission
        else
            if projects[sunet] < submission
                projects[sunet] = submission
            end
        end
    end
end

# Shuffle the submissions
sunets = projects.keys.shuffle!

# Initialize assignments hash with all the graders
assignments = Hash.new
graders.each do |grader|
    assignments[grader] = Array.new
end

# Divide assignments among the various TAs
currentGraderIndex = 0
sunets.each do |sunet|
    assignments[graders[currentGraderIndex]] << "submissions/#{options[:project]}/#{sunet}-#{projects[sunet]}"
    currentGraderIndex += 1
    if currentGraderIndex == graders.length
        currentGraderIndex = 0
    end
end

# Sends message to a person
def sendAssignments(grader, project, assignments, payload)

    # Construct the message
    message = "The following assignments have been assigned to you:\n\n"
    assignments.each do |assignment|
        message += "#{assignment}\n"
    end
    message += "\nIf there are any issues, please reply to this email."

    # Construct the email
    mail = Mail.new do
        from        "Roge Chen <rogchen@stanford.edu>"
        to          "#{grader}@stanford.edu"
        subject     "Grading assignments for Project #{project}"
        body        message 
        add_file    :filename => "p#{project}-#{grader}.zip", :content => File.read(payload)
    end
    mail.delivery_method :sendmail
    mail.deliver
end

# Generates ZIP file for each grader
def generateZipFile(grader, project, students)
    if (!File.directory?(PAYLOADS_TARGET))
        FileUtils.mkdir PAYLOADS_TARGET
    end
    `zip -r payloads/p#{project}-#{grader}.zip #{students.join(" ")}`
    return "payloads/p#{project}-#{grader}.zip"
end

# Utility for printing and flushing
def printAndFlush(str)
    print str
    $stdout.flush
end

# Send assignments to each grader
assignments.each do |grader, students|
    printAndFlush("Packaging assignments for #{grader}...")
    payload = generateZipFile(grader, options[:project], students)
    sendAssignments(grader, options[:project], students, payload)
    printAndFlush("done!\n")
end
